import utils
from sinks.phab import Client

PROJECT_PATCH_FOR_REVIEW_PHID = 'PHID-PROJ-onnxucoedheq3jevknyr'


class PhabricatorSink():
    def __init__(self, config):
        assert (config.get('type') == 'phabricator')

        self.url = config.get('url')
        self.user = config.get('user')
        self.token = config.get('token')

        if not self.url:
            raise RuntimeError(
                "The 'url' field must be populated for the phabricator sink")
        if not self.user:
            raise RuntimeError(
                "The 'user' field must be populated for the phabricator sink")
        if not self.token:
            raise RuntimeError(
                "The 'token' field must be populated for the phabricator sink")

        self.client = Client(self.url, self.user, self.token)

    def notify(self, event):
        kind = event.get('object_kind')

        # if kind == 'push':
        #   self._add_pushed_comments(event)
        # if kind == 'tag_push':
        #     self._add_tagged_comments(event)
        if kind == 'merge_request':
            self._handle_mr_event(event)

    def _add_pushed_comments(self, event):
        # Decide if we want to notify for this event:
        after = event.get('after', None)
        before = event.get('before', None)
        if before == '0000000000000000000000000000000000000000':
            # New branch, don't notify:
            return
        if after == before:
            # No changes, don't notify (unclear whether this can actually
            # happen):
            return

        for commit in event.get('commits'):
            message = self._push_message(commit, event)
            for task_id in utils.collect_task_ids(commit.get('message', '')):
                self._comment(task_id, message)

    def _handle_mr_event(self, event):
        object_attributes = event.get('object_attributes')
        if object_attributes is None:
            return
        action = object_attributes.get('action')
        if action is None:
            return

        if action in ['open', 'close', 'reopen', 'merge']:
            for task_id in utils.get_all_potential_task_ids_from_mr(object_attributes):
                self._add_mr_comment(task_id, action, event)

        elif action == 'update':
            # Notes:
            #
            # An MR change event is structured like so:
            #
            # {'changes': {'description': {'current': 'Adds ci to run tests\n\nBug: #323225',
            #                              'previous': 'Adds ci to run tests'},
            #              'last_edited_at': {'current': '2022-12-13 05:12:53 UTC',
            #                                 'previous': None},
            #              'last_edited_by_id': {'current': 12, 'previous': None},
            #              'updated_at': {'current': '2022-12-13 05:12:53 UTC',
            #                             'previous': '2022-12-13 05:09:52 UTC'},
            #              'updated_by_id': {'current': 12, 'previous': None}},
            #
            # We aren't checking commit messages here whatsoever.

            changes = event.get('changes')
            for task_id in utils.get_new_task_ids_from_mr_changes(changes):
                self._add_mr_comment(task_id, action, event)

    def _add_mr_comment(self, task_id, action, event):
        message = self._generate_mr_comment(task_id, action, event)
        self._comment(task_id, message)
        if action in ['open', 'reopen']:
            self._add_project_for_review(task_id)

    def _push_message(self, commit, event) -> str:
        user = event.get('user_username') or event.get(
            'user_name') or 'USERNAME MISSING'

        project = event.get('project')
        repo = "REPO INFORMATION MISSING"
        repo_url = "REPO URL MISSING"

        if project:
            x = project.get('path_with_namespace')
            if x:
                repo = x
            x = project.get('http_url') or project.get('web_url')
            if x:
                repo_url = x

        branch = utils.get_branch_from_ref(event['ref'])
        ref = commit['id']
        title = commit['title']
        link = commit['url']
        author = commit['author']['name']
        return f'''Related commit [[{link} | {ref[:8]}]] pushed by {user} (author: {author}):

[ [[{repo_url} | {repo}]]@{branch} ] {title}
'''

    def _generate_mr_comment(self, task_id, action, event) -> str:
        object_attributes = event.get('object_attributes')
        past_tense_action = utils.make_past_tense(action)

        who = event.get('user', {"username": "MISSING USER INFO"}).get(
            'username', 'MISSING USERNAME')
        mr_url = object_attributes.get('url', 'MISSING URL')
        title = object_attributes.get('title', 'MISSING TITLE')

        # FIXME: Mention source and target branches
        return f"""{who} {past_tense_action} {mr_url}

{title}
"""

    def _comment(self, task_id, message):
        self.client.comment(task_id, message)
        print(f"Added a comment on task {task_id}")

    def _add_project_for_review(self, task_id):
        # It is okay to attempt to add a project that has already been
        # added.  This just results in a null operation.
        self.client.addProject(task_id, PROJECT_PATCH_FOR_REVIEW_PHID)
