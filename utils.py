import re


def get_branch_from_ref(ref: str):
    """pull branch out of a refs/heads/* string, or return None"""
    if ref.startswith("refs/heads/"):
        return ref[len("refs/heads/"):]
    return None


def collect_task_ids(message: str) -> set:
    """Returns a set of task id strings"""
    task_ids = []
    task_ids.extend(
        ['T' + trailer for trailer in re.findall(r'^Bug: [#T](\d+)$', message, re.MULTILINE)])
    return set(task_ids)


def get_all_potential_task_ids_from_mr(object_attributes: dict) -> set:
    """Returns a set of task id strings"""
    desc = object_attributes.get('description', '')
    title = object_attributes.get('title', '')
    last_commit_message = object_attributes['last_commit']['message']

    return collect_task_ids(desc).union(collect_task_ids(title)).union(collect_task_ids(last_commit_message))


def get_new_task_ids_from_mr_changes(changes):
    """Returns a set of task id strings from a changed merge request."""
    task_ids_previous = set()
    task_ids_current = set()

    if changes.get('description'):
        task_ids_previous.update(collect_task_ids(changes['description']['previous']))
        task_ids_current.update(collect_task_ids(changes['description']['current']))

    if changes.get('title'):
        task_ids_previous.update(collect_task_ids(changes['title']['previous']))
        task_ids_current.update(collect_task_ids(changes['title']['current']))

    return task_ids_current.difference(task_ids_previous)


def make_past_tense(word: str) -> str:
    if word[-1] == "e":
        return word + "d"
    else:
        return word + "ed"
