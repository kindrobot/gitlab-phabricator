####

lint: lint-using-docker

lint-ci: lint-using-docker-bundled

##

fix: fix-using-docker

lint-local:
	tox

LINT_IMAGE = localhost:5000/gitlab-phabricator/lint:latest

lint-image:
	docker build --target lint-base -t $(LINT_IMAGE) -f Dockerfile.lint .

# This one is fastest for local testing
lint-using-docker: lint-image
	docker run --rm -v $$(pwd):/source:ro $(LINT_IMAGE)

# The linter is run during the build of this image.  This is the one to use in CI jobs
lint-using-docker-bundled:
	docker build --target code-and-linter -f Dockerfile.lint --build-arg CACHEBUSTER=$$(date -Ins) .

fix-using-docker: lint-image
	docker run --rm -v $$(pwd):/source:rw --user $$(id -u):$$(id -g) --entrypoint /data/tox/lint/bin/autopep8 $(LINT_IMAGE) -r --in-place .

####

